<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Developer Data</title>
</head>
<body>
<h1>Developer Data</h1>

<table>
    <tr>
        <td>ID</td>
        <td>${id}</td>
    </tr>

    <tr>
        <td>First Name</td>
        <td>${firstName}</td>
    </tr>

    <tr>
        <td>Last Name</td>
        <td>${lastName}</td>
    </tr>

    <tr>
        <td>Specialty</td>
        <td>${specialty}</td>
    </tr>

    <tr>
        <td>Salary</td>
        <td>${salary}</td>
    </tr>
</table>
</body>
</html>
