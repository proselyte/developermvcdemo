<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Create Developer</title>
</head>
<body>

<h1>Add developer data</h1>
<form:form method="post" action="addDeveloper">
    <table>
        <tr>
            <td><form:label path="id">ID</form:label></td>
            <td><form:input path="id"/>
        </tr>

        <tr>
            <td><form:label path="firstName">First Name</form:label></td>
            <td><form:input path="firstName"/>
        </tr>

        <tr>
            <td><form:label path="lastName">Last Name</form:label></td>
            <td><form:input path="lastName"/>
        </tr>

        <tr>
            <td><form:label path="specialty">Specialty</form:label></td>
            <td><form:input path="specialty"/>
        </tr>

        <tr>
            <td><form:label path="salary">Salary</form:label></td>
            <td><form:input path="salary"/>
        </tr>

        <tr>
            <td><input type="submit" value="Submit"></td>
        </tr>
    </table>
</form:form>
</body>
</html>
